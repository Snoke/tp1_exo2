"""
Création et Test de la class du calculateur
"""

class Operation:
    """
    Classe définissant les opérations mathématiques habituelles
    """
    result = 0
    def addition(self, a_1, b_1):
        """
        Fonction d'addition de deux entiers a et b
        """
        self.result = a_1 + b_1
        print(self.result)

    def multiplication(self, a_2, b_2):
        """
        Fonction de multiplication de deux entiers a et b
        """
        self.result = a_2 * b_2
        print(self.result)

    def soustraction(self, a_3, b_3):
        """
            Fonction de soustraction de deux entiers a et b
        """
        self.result = a_3 - b_3
        print(self.result)

    def division(self, a_4, b_4):
        """
            Fonction de division de deux entiers a et b
        """
        self.result = a_4 / b_4
        print(self.result)


OPERATEUR = Operation()
VAR_1 = 45
VAR_2 = 15

OPERATEUR.addition(VAR_1, VAR_2)
OPERATEUR.division(VAR_1, VAR_2)
OPERATEUR.multiplication(VAR_1, VAR_2)
OPERATEUR.division(VAR_1, VAR_2)
